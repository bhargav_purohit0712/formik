import React from 'react'

const error = props => {
  return <div className='error'>{props.children}</div>
}

export default error
