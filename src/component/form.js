import React from 'react'

import * as Yup from 'yup'
import { Form, Formik } from 'formik'

import FormikController from './controller'

function form () {
  const dropdownOptions = [
    { key: 'Select an option', value: '' },
    { key: 'Option 1', value: 'option1' },
    { key: 'Option 2', value: 'option2' },
    { key: 'Option 3', value: 'option3' }
  ]

  const radioOptions = [
    { key: 'male', value: 'male' },
    { key: 'female', value: 'female' }
  ]

  const checkOptions = [
    { key: 'react', value: 'react' },
    { key: 'node', value: 'node' }
  ]

  const initalValue = {
    email: '',
    desc: '',
    selop: '',
    gen: '',
    chk: [],
    bdate: null
  }

  const validationSchema = Yup.object({
    email: Yup.string()
      .email('Invalid email format')
      .required('Required'),
    desc: Yup.string().required('Required'),
    selop: Yup.string().required('Required'),
    gen: Yup.string().required('Required'),
    chk: Yup.array().required('Required'),
    bdate: Yup.date()
      .required('Required')
      .nullable()
  })

  const submit = val => {
    console.log('data:-', val)
  }

  return (
    <Formik
      initialValues={initalValue}
      validationSchema={validationSchema}
      onSubmit={submit}
    >
      {formik => (
        <Form>
          <FormikController
            controller='input'
            type='email'
            label='email'
            name='email'
          />
          <FormikController
            controller='textarea'
            label='Description'
            name='desc'
          />
          <FormikController
            controller='select'
            label='Select Any'
            name='selop'
            options={dropdownOptions}
          />
          <FormikController
            controller='radio'
            label='Gender'
            name='gen'
            options={radioOptions}
          />
          <FormikController
            controller='checkbox'
            label='Cources'
            name='chk'
            options={checkOptions}
          />
          <FormikController
            controller='date'
            label='Birthdate'
            name='bdate'
          />

          <button type='submit'>Register</button>
        </Form>
      )}
    </Formik>
  )
}

export default form
