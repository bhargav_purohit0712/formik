import React from 'react'

import { Field, ErrorMessage } from 'formik'
import Error from './error'

const radio = props => {
  const { label, name, options, ...rest } = props
  return (
    <div className='form-control'>
      <label htmlFor={name}>{label}</label>
      <Field name={name} {...rest}>
        {({ field }) =>
          !options
            ? null
            : options.map(op => (
                <React.Fragment key={op.key}>
                  <input
                    type='radio'
                    id={op.value}
                    {...field}
                    value={op.value}
                    checked={field.value === op.value}
                  />
                  <label htmlFor={op.value}>{op.key}</label>
                </React.Fragment>
              ))
        }
      </Field>
      <ErrorMessage name={name} component={Error} />
    </div>
  )
}

export default radio
