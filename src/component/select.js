import React from 'react'

import { Field, ErrorMessage } from 'formik'
import Error from './error'

const select = props => {
  const { label, name, options, ...rest } = props
  return (
    <div className='form-control'>
      <label htmlFor={name}>{label}</label>
      <Field as='select' name={name} id={name} {...rest}>
        {options.map(op => (
          <option key={op.value} value={op.value}>
            {op.key}
          </option>
        ))}
      </Field>
      <ErrorMessage name={name} component={Error} />
    </div>
  )
}
export default select
