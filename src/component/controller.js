import React from 'react'

import Input from './input'
import Textarea from './Textarea'
import Select from './select'
import Radio from './radio'
import Checkbox from './checkbox'
import Date from './date'

function Formikcontroller (props) {
  const { controller, ...rest } = props
  switch (controller) {
    case 'input':
      return <Input {...rest} />
    case 'textarea':
      return <Textarea {...rest} />
    case 'select':
      return <Select {...rest} />
    case 'radio':
      return <Radio {...rest} />
    case 'checkbox':
      return <Checkbox {...rest} />
    case 'date':
      return <Date {...rest} />

    default:
      return null
  }
}

export default Formikcontroller
