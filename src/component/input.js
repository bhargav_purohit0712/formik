import React from 'react'

import { Field, ErrorMessage } from 'formik'
import Error from './error'

const input = props => {
  const { label, name, ...rest } = props
  return (
    <div className='form-control'>
      <label htmlFor={name}>{label}</label>
      <Field name={name} id={name} {...rest} />
      <ErrorMessage name={name} component={Error} />
    </div>
  )
}

export default input
